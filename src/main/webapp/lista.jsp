<%-- 
    Document   : index
    Created on : 02-05-2021, 21:25:34
    Author     : itorres

--%>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.evaluacionFinal.entity.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
      List<Diccionario> lista   = (List<Diccionario>) request.getAttribute("lista");
    Iterator<Diccionario> itPalabra = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="DiccionarioController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>fecha</th>
                    <th>palabra Consultada </th>
                    <th>Significado </th><!-- comment -->
                    <th> </th>
        
                    </thead>
                    <tbody>
                        <%while (itPalabra.hasNext()) {
                       Diccionario pal = itPalabra.next();%>
                        <tr>
                            <td><%= pal.getFechaconsulta()%></td>
                            <td><%= pal.getPalabraconsultada()%></td>
                            <td><%= pal.getSignificadopalabraconsultada()%></td>
                    <br>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
              

        
        </form>
    </body>
</html>
