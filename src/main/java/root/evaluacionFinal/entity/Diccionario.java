/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacionFinal.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ignacio
 */
@Entity
@Table(name = "diccionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Diccionario.findAll", query = "SELECT d FROM Diccionario d"),
    @NamedQuery(name = "Diccionario.findBySignificadopalabraconsultada", query = "SELECT d FROM Diccionario d WHERE d.significadopalabraconsultada = :significadopalabraconsultada"),
    @NamedQuery(name = "Diccionario.findByPalabraconsultada", query = "SELECT d FROM Diccionario d WHERE d.palabraconsultada = :palabraconsultada"),
    @NamedQuery(name = "Diccionario.findByFechaconsulta", query = "SELECT d FROM Diccionario d WHERE d.fechaconsulta = :fechaconsulta")})
public class Diccionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "significadopalabraconsultada")
    private String significadopalabraconsultada;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "palabraconsultada")
    private String palabraconsultada;
    @Size(max = 2147483647)
    @Column(name = "fechaconsulta")
    private String fechaconsulta;

    public Diccionario() {
    }

    public Diccionario(String palabraconsultada) {
        this.palabraconsultada = palabraconsultada;
    }

    public String getSignificadopalabraconsultada() {
        return significadopalabraconsultada;
    }

    public void setSignificadopalabraconsultada(String significadopalabraconsultada) {
        this.significadopalabraconsultada = significadopalabraconsultada;
    }

    public String getPalabraconsultada() {
        return palabraconsultada;
    }

    public void setPalabraconsultada(String palabraconsultada) {
        this.palabraconsultada = palabraconsultada;
    }

    public String getFechaconsulta() {
        return fechaconsulta;
    }

    public void setFechaconsulta(String fechaconsulta) {
        this.fechaconsulta = fechaconsulta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (palabraconsultada != null ? palabraconsultada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Diccionario)) {
            return false;
        }
        Diccionario other = (Diccionario) object;
        if ((this.palabraconsultada == null && other.palabraconsultada != null) || (this.palabraconsultada != null && !this.palabraconsultada.equals(other.palabraconsultada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.evaluacionFinal.entity.Diccionario[ palabraconsultada=" + palabraconsultada + " ]";
    }
    
}
