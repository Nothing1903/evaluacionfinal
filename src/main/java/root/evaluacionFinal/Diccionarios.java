/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacionFinal;
import cl.dto.PalabraDTO;
import cl.dto.Metadata;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonReader;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.diccionario.dao.DiccionarioJpaController;
import root.evaluacionFinal.dao.exceptions.NonexistentEntityException;
import root.evaluacionFinal.entity.Diccionario;

/**
 *
 * @author itorres
 */
@Path("palabras")
public class Diccionarios {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPalabras() {
        DiccionarioJpaController dao = new DiccionarioJpaController();

        List<Diccionario> lista = dao.findDiccionarioEntities();

        return Response.ok(200).entity(lista).build();

    }
    @GET
    @Path("/(idbuscar)")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
            //Diccionario palabra = new Diccionario();
            //palabra.setPalabraconsultada(request.getParameter("palabra"));
            //palabra.setFechaconsulta(new Date().toString());
            //palabra.setSignificadopalabraconsultada("null");
            
            Client client =ClientBuilder.newClient();
            WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+ idbuscar);   
            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_key", "016e54febbe5acbb64d4484138955255").header("app_id", "da8b0caf").get(PalabraDTO.class);         
            Diccionario palabra = new Diccionario();
            palabra.setPalabraconsultada(palabradto.getId());
            palabra.setFechaconsulta(new Date().toString());
            palabra.setSignificadopalabraconsultada(palabradto.getWord());
            return Response.ok(200).entity(palabra).build();
//request.getRequestDispatcher("index.jsp").forward(request, response);  
    } 

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Diccionario palabra) {

        try {
            DiccionarioJpaController dao = new DiccionarioJpaController();
            dao.create(palabra);
        } catch (Exception ex) {
            Logger.getLogger(Diccionario.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(palabra).build();

    }

}
