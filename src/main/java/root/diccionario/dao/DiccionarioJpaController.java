/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.diccionario.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.evaluacionFinal.dao.exceptions.NonexistentEntityException;
import root.evaluacionFinal.dao.exceptions.PreexistingEntityException;
import root.evaluacionFinal.entity.Diccionario;

/**
 *
 * @author itorres
 */
public class DiccionarioJpaController implements Serializable {

    public DiccionarioJpaController() {
 
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Diccionario palabra) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(palabra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDiccionario(palabra.getPalabraconsultada()) != null) {
                throw new PreexistingEntityException("Diccionario " + palabra + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    

    public List<Diccionario> findDiccionarioEntities() {
        return findDiccionarioEntities(true, -1, -1);
    }

    public List<Diccionario> findDiccionarioEntities(int maxResults, int firstResult) {
        return findDiccionarioEntities(false, maxResults, firstResult);
    }

    private List<Diccionario> findDiccionarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Diccionario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Diccionario findDiccionario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Diccionario.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiccionarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Diccionario> rt = cq.from(Diccionario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
