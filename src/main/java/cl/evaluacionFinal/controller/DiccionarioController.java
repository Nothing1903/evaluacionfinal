/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.evaluacionFinal.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import root.diccionario.dao.DiccionarioJpaController;
import root.evaluacionFinal.Diccionarios;
import root.evaluacionFinal.entity.Diccionario;

/**
 *
 * @author itorres
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PalabraController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            System.out.println("POST");
        String accion = request.getParameter("accion");

        if (accion.equals("registrarpalabra")) {
            Diccionario palabra = new Diccionario();
            //Diccionarios diccionario = new Diccionarios();
            //diccionario.significado(request.getParameter("palabra"));
            palabra.setPalabraconsultada(request.getParameter("palabra"));
            palabra.setFechaconsulta(new Date().toString());
            palabra.setSignificadopalabraconsultada("Not Found");
            Client client =ClientBuilder.newClient();
            WebTarget myResource = client.target("https://diccionarioevaluacion.herokuapp.com/api/palabras");
            Diccionario palabra1 = myResource.request(MediaType.APPLICATION_JSON).post(Entity.json(palabra),Diccionario.class);
            request.getRequestDispatcher("index.jsp").forward(request, response);    
        }
        if (accion.equals("listar")) {  
            Client client =ClientBuilder.newClient();
            WebTarget myResource = client.target("https://diccionarioevaluacion.herokuapp.com/api/palabras");
            List<Diccionario> lista =(List<Diccionario>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Diccionario>>(){});
            request.setAttribute("lista", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);            
        } 
         
    }
        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
